﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SpectrumTest.Data.Models;
using SpectrumTest.Data.Repositories;

namespace SpectrumTest.Managers
{
	public class UserManager
    {
        UserRepository _userRepo;

        public UserManager()
        {
            _userRepo = new UserRepository();
        }

        /// <summary>
        /// Get all users async
        /// </summary>
        /// <returns></returns>
        public async Task<IList<User>> GetUsersAsync()
        {
            IList<User> users = null;
            try
            {
                users = await _userRepo.GetAllAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return users;
        }

        /// <summary>
        /// Save user async
        /// </summary>
        /// <param name="user">User object</param>
        /// <returns></returns>
        public async Task SaveUserAsync(User user)
        {
            try
            {
                await _userRepo.SaveAsync(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}