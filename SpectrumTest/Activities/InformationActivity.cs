﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SpectrumTest.Activities
{
	[Activity(Label = "Requirements", Theme = "@style/AppTheme")]
	public class InformationActivity : BaseActivity
	{
		protected override int ResourceLayout
		{
			get { return Resource.Layout.information_layout; }
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);


		}
	}
}
