﻿
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using SpectrumTest.Data;

namespace SpectrumTest
{
	[Activity(Label = "BaseActivity")]
	public abstract class BaseActivity : AppCompatActivity
	{

		protected abstract int ResourceLayout { get; }      
		public Toolbar Toolbar { get; set; }
		public int ActionBarIcon { set { Toolbar.SetNavigationIcon(value); } }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(ResourceLayout);

			ConnectionManager.Instance.Initialize();

			if (Toolbar != null)
			{
				SetSupportActionBar(Toolbar);            
				SupportActionBar.SetDisplayHomeAsUpEnabled(true);
				SupportActionBar.SetHomeButtonEnabled(true);
			}

		}
	}

}
