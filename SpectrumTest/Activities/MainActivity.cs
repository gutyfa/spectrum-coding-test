﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V7.Widget;
using SpectrumTest.Activities.Adapters;
using Core.Model;
using Android.Content;
using SpectrumTest.Activities;
using SpectrumTest.Data.Repositories;
using System.Linq;
using SpectrumTest.Data;
using Android.Util;
using Android.Runtime;
using System.Collections.Generic;
using SpectrumTest.Data.Models;
using Android.Support.Design.Widget;
using System.Threading.Tasks;

namespace SpectrumTest
{
	[Activity(Label = "Spectrum conding test", MainLauncher = true, Icon = "@mipmap/icon", Theme = "@style/AppTheme")]
	public class MainActivity : BaseActivity
	{
		protected override int ResourceLayout { get { return Resource.Layout.Main; } }
		RecyclerView recyclerView;
		//RecyclerView.Adapter recyclerviewUserAdapter;
		UserAdapter recyclerviewUserAdapter;
		RecyclerView.LayoutManager layoutManager;
		Managers.UserManager manager;
		DividerItemDecoration itemDecoration;
		RecyclerView.ItemAnimator itemAnimator;
		IList<User> users;
		FloatingActionButton floatingActionButton;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
			SupportActionBar.SetDisplayHomeAsUpEnabled(false);
			SupportActionBar.SetHomeButtonEnabled(false);

			// Getting componentes
			recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
			floatingActionButton = FindViewById<FloatingActionButton>(Resource.Id.add_user_floating);

			//var userText = FindViewById<TextView>(Resource.Id.titleRecyclerView);         

			// Init repo
			manager = new SpectrumTest.Managers.UserManager();
			Task.Run(async () =>
			{
				users = await manager.GetUsersAsync().ConfigureAwait(false);

				// Run On UI Context
				RunOnUiThread(() =>
				{
					// Settings RecyclerView

					// Setting How the Recyclerview is looks like
					layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);

					// Setting layout manager to the recyclerview
					recyclerView.SetLayoutManager(layoutManager);

					// Setting Decorator
					itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.Vertical);
					recyclerView.AddItemDecoration(itemDecoration);

					// Setting animator
					itemAnimator = new DefaultItemAnimator();
					recyclerView.SetItemAnimator(itemAnimator);

					//Setting Adapter
					recyclerviewUserAdapter = new UserAdapter(users);

					// Setting Event Click adapter
					recyclerviewUserAdapter.ItemClick += OnClick;

					//Inject adapter
					recyclerView.SetAdapter(recyclerviewUserAdapter);
				});
			});


			// Event new User

			floatingActionButton.Click += delegate
			{
				var intent = new Intent(this, typeof(AddUserActivity));
				StartActivityForResult(intent, 100);
			};
		}


		void OnClick(object sender, int position)
		{
			Toast.MakeText(this, "Click on item" + position, ToastLength.Short).Show();
		}

		// Inflate Menu to the toolbar
		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate(Resource.Layout.top_menu, menu);
			return base.OnCreateOptionsMenu(menu);
		}

		// Ability to selected an item of Toolbar's menu
		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Resource.Id.add_new_user:
					var intent = new Intent(this, typeof(InformationActivity));
					StartActivity(intent);
					break;
			}
			return base.OnOptionsItemSelected(item);
		}
		protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
		{
			if (requestCode == 100 && resultCode == Result.Ok)
			{
				string username = data.GetStringExtra("username");
				users.Add(new User() { Username = username });
				recyclerviewUserAdapter.NotifyDataSetChanged();
			}
		}
	}
}

