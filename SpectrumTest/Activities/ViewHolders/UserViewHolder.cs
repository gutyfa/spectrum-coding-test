﻿using System;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace SpectrumTest.Activities.ViewHolders
{
    
	public class UserViewHolder : RecyclerView.ViewHolder
	{
		public TextView UserName { get; private set; }

		public UserViewHolder(View itemView, Action<int> listener) : base(itemView)
		{
			// Locate and cache view references         
			UserName = itemView.FindViewById<TextView>(Resource.Id.user_name);
			itemView.Click += (sender, e) => listener(base.LayoutPosition);
		}
	}
}
