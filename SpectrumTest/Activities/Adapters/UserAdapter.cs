﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using Core.Model;
using SpectrumTest.Activities.ViewHolders;
using SpectrumTest.Data.Models;

namespace SpectrumTest.Activities.Adapters
{
	public class UserAdapter : RecyclerView.Adapter
	{
		IList<User> users;
		public event EventHandler<int> ItemClick;      

		public UserAdapter(IList<User> users)
		{
			this.users = users;
		}

		public override int ItemCount
		{
			get {
				return users == null ? 0 : users.Count;            
			}
		}

		// Replace the contents of a view (invoked by the layout manager)
		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			var item = users[position];

			// Replace the contents of the view with that element
			var vh = holder as UserViewHolder;
			vh.UserName.Text = item.Username;
	
		}

        // Inflate Adapter 
		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View item = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_recyclerview, parent, false);         
			return new UserViewHolder(item, OnClick);
		}

		private void OnClick(int position)
		{
			if (ItemClick != null)
				ItemClick(this, position);
		}
	}
}
