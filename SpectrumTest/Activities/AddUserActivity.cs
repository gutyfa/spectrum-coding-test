﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using SpectrumTest.Data.Models;

namespace SpectrumTest.Activities
{
	[Activity(Label = "@string/add_user_title", Theme = "@style/AppTheme")]
	public class AddUserActivity : BaseActivity
	{
		EditText username, password;
		Button button;
		Managers.UserManager manager;

		protected override int ResourceLayout
		{
			get { return Resource.Layout.AddNewUserLayout; }
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Getting component
			username = FindViewById<EditText>(Resource.Id.new_user);
			password = FindViewById<EditText>(Resource.Id.new_user_password);
			button = FindViewById<Button>(Resource.Id.save_button);

			manager = new Managers.UserManager();

			button.Click += async delegate
			{
				var user = new User() { Username = username.Text, Password = password.Text };
				var intent = new Intent();


				if (!ValidateUserName(username.Text))
				{
					username.Error = "Enter a username";
					return;
				}

				if (!Helpers.PasswordHelper.IsValidPassword(password.Text))
				{
					password.Error = "Enter a valid password";
					return;
				}

				await manager.SaveUserAsync(user);
				intent.PutExtra("username", username.Text);
				SetResult(Result.Ok, intent);
				Finish();

			};
		}

		bool ValidateUserName(string username)
		{
			if (Equals(username, ""))
			{
				return false;
			}
			return true;
		}
	}
}
