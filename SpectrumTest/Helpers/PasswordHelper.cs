﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SpectrumTest.Helpers
{
    public static class PasswordHelper
    {
        public static bool IsValidPassword(string password)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasLetter = new Regex(@"[a-zA-Z]+");
            var hasMiniMaxChars = new Regex(@".{5,12}");
            var isAlphaNumeric = new Regex(@"^[a-zA-Z0-9]+$");

            if (!hasNumber.IsMatch(password)
                || !hasLetter.IsMatch(password)
                || !hasMiniMaxChars.IsMatch(password)
                || !isAlphaNumeric.IsMatch(password)
                || HasSequence(password))
                return false;

            return true;
        }

        private static bool HasSequence(string password)
        {
            var sequenceFound = false;
            var maxSequenceNumber = password.Length / 2;

            for (int i = 1; i <= maxSequenceNumber; i++)
            {
                sequenceFound = FindSequence(password, i);
                if (sequenceFound)
                    break;
            }

            return sequenceFound;
        }

        private static bool FindSequence(string password, int sequenceNumber)
        {
            while ((password.Length / sequenceNumber) >= 2)
            {
                var sequence = password.Substring(0, sequenceNumber);
                var nextSequence = password.Substring(sequenceNumber, sequenceNumber);

                if (sequence == nextSequence)
                    return true;

                password = password.Substring(1);
            }

            return false;
        }
    }
}