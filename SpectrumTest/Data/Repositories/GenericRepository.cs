﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SpectrumTest.Data.Models;
using SQLite;

namespace SpectrumTest.Data.Repositories
{
    public abstract class GenericRepository<T> where T : BaseModel, new ()
    {
        protected SQLiteAsyncConnection ConnectionAsync { get; private set; }

        public GenericRepository()
        {
            //Get connection
            ConnectionAsync = ConnectionManager.Instance.GetAsyncConnection();
            //Create the table according with the specific implementation
            ConnectionAsync.CreateTableAsync<T>();
        }

        public virtual async Task SaveAsync(T itemToInsert)
        {
            if (itemToInsert == null)
                throw new NullReferenceException("SaveAsync must have a value to insert SQLite");

            await ConnectionAsync.InsertAsync(itemToInsert).ConfigureAwait(false);
        }

        public async Task<IList<T>> GetAllAsync(CancellationToken? token = null)
        {
            if (token.HasValue)
            {
                token.Value.ThrowIfCancellationRequested();
            }

            IList<T> items = await ConnectionAsync.Table<T>().ToListAsync().ConfigureAwait(false);
            return items;
        }
    }
}