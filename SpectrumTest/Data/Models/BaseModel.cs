﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace SpectrumTest.Data.Models
{
    public class BaseModel
    {
        /// <summary>
        /// The model indentifier.
        /// </summary>
        [PrimaryKey, AutoIncrement]
        public int ModelId { get; set; }
    }
}