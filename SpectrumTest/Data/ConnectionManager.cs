﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace SpectrumTest.Data
{
    public class ConnectionManager
    {
        const string DatabaseName = "db_test";
        string _path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        

        //Singleton
        static readonly Lazy<ConnectionManager> _instance = new Lazy<ConnectionManager>(() => new ConnectionManager());
        public static ConnectionManager Instance { get { return _instance.Value; } }
        //SQLite Async
        SQLiteAsyncConnection _sqliteAsyncConnection;

        private ConnectionManager() { }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            if (_sqliteAsyncConnection == null)
                throw new Exception("ConnectionManager.Instance.Initialize could not load check Android Main Activity");

            return _sqliteAsyncConnection;
        }

        public void Initialize()
        {
            //Create just one connection
            if (_sqliteAsyncConnection == null)
            {
                var databasePath = System.IO.Path.Combine(_path, DatabaseName);
                _sqliteAsyncConnection = new SQLiteAsyncConnection(databasePath);
            }
        }
    }
}