﻿using System;
using System.Collections.Generic;
using SpectrumTest.Data.Models;

namespace Core.Model
{
	/// <summary>
    /// Dummy information class 
	/// This class it is just for return a dummy User information.
    /// </summary>
	public class DummyInformation
	{
		public static List<User> GetUsersDummy()
		{
			var user = new List<User>();

			user.Add(new User { Username = "John Doe", Password = "Password1." });
			user.Add(new User { Username = "John Doe 1", Password = "Password1." });
			user.Add(new User { Username = "John Doe 2", Password = "Password1." });
			user.Add(new User { Username = "John Doe 3", Password = "Password1." });
			user.Add(new User { Username = "John Doe 3", Password = "Password1." });

			return user;
		}
	}
}
